#Python script to generarate a graph using BA (Barabasi Albert) algorithm and save using cPickle
#Be careful of transferring data bw machines - cPickle not entirely secure
import numpy as np
import copy
import cPickle
import datetime
import os
import sys
import readline


class BA_Graph:

	def __init__(self):
		self.edges_from_to=[] #tuples to be stored here
		self.edges_to_from=[] #tuples to be stored here
		self.vertices=[]

		self.max_vertex_index=0 #the beginning of the indexing system for the graph
		self.OUTPUT_VERTICES=10 #how many output vertices we want (this particular task used MNIST so 10 outputs was chosen)

		self.degrees=[] #a list of the degree of each vertex
		self.distances=[] #a list of how far away each node is from the end by number of nodes
		self.disteliste=[] #a list of the numbers of all the vertices by distance from output

		self.sum_degrees=""
		self.probabilities = ""
		self.cumulative_probabilities=""
		self.list_of_to_vertices = ""
		self.current_vertex=""
		self.noi=0
		print ""
		print ""
		print 'Welcome to the Scale Free NN for Alzheimer Research!'
		print 'First select number of input vertices required and the M number of barabassi-albert process,'
		print 'Then call <class instance name>.generate_connections() to populate network structure'
		print 'Then call <class instance name>.save_graph() to save the graph in working directory'
		print 'Optionally, you can call <class instance name>.load_graph() to load a previously-generated graph'
		print ""
		print ""

		self.initialise()

	def initialise(self):
		nin = int(raw_input("How many input vertices? "))

		self.set_num_inputs(nin)

		nedge = int(raw_input("What is m number of BA model? Must be int >=2, sets the amount of links added for each vertex added: "))
		self.set_num_edges(nedge)

		dists=[]

		for k in range(self.OUTPUT_VERTICES):
			dists.append(self.max_vertex_index)
			self.vertices.append(self.max_vertex_index)
			self.degrees.append(0)
			self.max_vertex_index+=1
			self.degrees.append(0)
			self.distances.append(0) #for the output vertices

		self.disteliste.append(dists)

		dists=[]
		for k in range(self.OUTPUT_VERTICES):
			self.distances.append(1) # for the ones just before output vertices
			self.vertices.append(self.max_vertex_index)
			dists.append(self.max_vertex_index)

			for i in range(self.OUTPUT_VERTICES):
				eft=[]
				etf=[]
				edge_from=self.vertices[-1]
				edge_to=i #i loops from 0 to 10

				self.degrees[edge_from]+=1
				self.degrees[edge_to]+=1
				eft.append(edge_from)
				eft.append(edge_to)

				etf.append(edge_to)
				etf.append(edge_from)

				self.edges_from_to.append(eft)
				self.edges_to_from.append(etf)

			self.max_vertex_index+=1
		self.disteliste.append(dists)
		self.max_vertex_index-=1	
		#return max_vertex_index,edges_from_to,degrees,edges_to_from,disteliste
		return self

	def return_sum_degrees(self):
		self.sum_degrees=0
		for k in range(10,len(self.vertices)):
			self.sum_degrees+=self.degrees[k]
		return self

	def return_probabilities(self):
		self.probabilities=[]
		for k in range(self.OUTPUT_VERTICES):
			self.probabilities.append(0)
		for k in range(self.OUTPUT_VERTICES,len(self.vertices)):
			self.probabilities.append(float(self.degrees[k])/self.sum_degrees)
		return self

	def sum_probabilities(self):
		self.cumulative_probabilities=copy.deepcopy(self.probabilities)
		for k in range(len(self.probabilities)-1):
			self.cumulative_probabilities[k+1]+=self.cumulative_probabilities[k]
		self.cumulative_probabilities[-1]=1
		return self

	def add_vertex(self):
		self.max_vertex_index+=1
		self.vertices.append(self.max_vertex_index)
		self.degrees.append(0) # for new vertex, but no degree yet cos no edges from this one
		return self

	def add_edges(self):
		edge_from=self.vertices[-1]

		for k in range(len(self.list_of_to_vertices)):
			eft=[]
			etf=[]
			edge_from=self.vertices[-1]
			edge_to=self.list_of_to_vertices[k] #i loops from 0 to 10
			self.degrees[edge_from]+=1
			self.degrees[edge_to]+=1
			eft.append(edge_from)
			eft.append(edge_to)
			etf.append(edge_to)
			etf.append(edge_from)
			self.edges_from_to.append(eft)
			self.edges_to_from.append(etf)

		return self

	def return_num_one_input(self):
		self.noi=0
		for v in self.vertices:
			if self.degrees[v]==self.number_of_edges:
				self.noi+=1

		return self

	def choose_edges(self):
		self.list_of_to_vertices=[]
		while len(self.list_of_to_vertices)<self.number_of_edges:
			prob = np.random.uniform(0,1)
			for k in range(len(self.cumulative_probabilities)-1):
				if prob>self.cumulative_probabilities[k]:
					if prob<=self.cumulative_probabilities[k+1]:
						in_list=False
						for j in range(len(self.list_of_to_vertices)):
							if self.list_of_to_vertices[j]==k+1:	#k is already in the list sao pauloe
								in_list=True

						if in_list:
							pass
						else:
							self.list_of_to_vertices.append(k+1) #append the index

		return self

	def set_num_inputs(self,num_inputs):
		self.num_inputs = num_inputs
		return self

	def set_num_edges(self,num_edges):
		self.number_of_edges = num_edges
		return self

	def generate_connections(self):
		self.noi = 0
		np.random.seed(1)
		while self.noi<self.num_inputs:
			self.return_sum_degrees()
			self.return_probabilities()
			self.sum_probabilities()
			self.choose_edges()
			self.add_vertex()
			self.add_edges()
			self.return_num_one_input()
		return self

	def save_graph(self):
		now = datetime.datetime.now()
		out_fn = 'BA_Graph' + now.strftime("%Y-%m-%d %H:%M")
		file = open(out_fn+'.txt','w')
		file.write(cPickle.dumps(self.__dict__))
		file.close()

	def load_graph(self):
		files = [f for f in os.listdir('.') if os.path.isfile(f)]
		qualifying_files = [f for f in files if 'BA_Graph' in f and '.txt' in f]
		qualifying_fns=[f[len('BA_Graph'):(len(f)-len('.txt'))] for f in qualifying_files]
		datetime_objects = [datetime.datetime.strptime(f, "%Y-%m-%d %H:%M") for f in qualifying_fns]
		chosen_index=datetime_objects.index(max(datetime_objects))
		chosen_fn = qualifying_files[chosen_index]
		f = open(chosen_fn, 'rb')
		tmp_dict = cPickle.load(f)
		f.close()          
		self.__dict__.update(tmp_dict) 
		return self

		