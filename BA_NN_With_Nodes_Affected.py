'This module removes or adds neurons to the trained network'

#------------------------------------------------------
#Import Libraries
#------------------------------------------------------
from BA_NN import *


class BA_NN_With_Nodes_Affected(BA_NN):

	def return_network_properties(self):
		self.output_neurons = [n for n in self.neurons if len(self.etf[n])>0 if len(self.eft[n])==0]
		self.input_neurons = [n for n in self.neurons if len(self.etf[n])==0 if len(self.eft[n])>0]
		self.degrees = [len(self.etf[n])+len(self.eft[n]) for n in self.neurons]
		self.non_zero_neurons = [n for n in self.neurons if self.degrees[n] > 0]
		return self

	def add_neuron(self):
		self.return_network_properties()
		list_of_additions=[np.random.choice(self.output_neurons),np.random.choice(self.input_neurons)]
		'Find the non-zero neurons and cumulative degrees!'

		self.calculate_cumulative_degrees()
		#degrees added now so...choose....
		number_of_added_neurons=2

		while number_of_added_neurons<=self.number_of_edges: #number_of_edges is BA number of current graph model...
			reset = False
			rand_choose=np.random.randint(0,self.cumulative_degrees[-1])

			if rand_choose<=self.cumulative_degrees[0]:
				chosen_neuron = non_zero_neurons[0]
				chosen_neuron_index=0

				for added_neuron in list_of_additions:
					if non_zero_neurons[0]==added_neuron:
						reset=True

				if reset==False:
					list_of_additions.append(chosen_neuron)
					reset=True

			if reset==False:
				for n in range(1,len(self.cumulative_degrees)):
					if rand_choose<=self.cumulative_degrees[n] and rand_choose>self.cumulative_degrees[n-1]:
							chosen_neuron=self.non_zero_neurons[n]
							chosen_neuron_index=n

				if chosen_neuron in list_of_additions:
					reset=True

				if reset==False:
					list_of_additions.append(chosen_neuron)

			number_of_added_neurons = len(list_of_additions)

		to_list=[]
		from_list=[]

		to_list.append(list_of_additions[0])
		from_list.append(list_of_additions[1])

		new_neuron = self.neurons[-1]+1 #this is neuron index

		self.neurons.append(new_neuron)

		'Now populate old weights and biases'
		weights_new=np.zeros((self.NUM_OF_NEURONS+1,self.NUM_OF_NEURONS+1))

		dummy_weights = copy.deepcopy(self.weights_from_to)

		for from_neuron in xrange(self.NUM_OF_NEURONS):
			for to_neuron in xrange(self.NUM_OF_NEURONS):
				weights_new[from_neuron][to_neuron]=dummy_weights[from_neuron][to_neuron]

		self.biases.append(np.random.uniform(-1,1))

		'Now do weights'
		weights_new[list_of_additions[1]][new_neuron-1]=np.random.uniform(-1,1) # -1 for list index...

		edges_from=[]

		edges_from.append(list_of_additions[1])

		self.etf.append(edges_from)

		weights_new[new_neuron-1][list_of_additions[0]]=np.random.uniform(-1,1)

		edges_to=[]

		edges_to.append(list_of_additions[0])

		for k in xrange(2,len(list_of_additions)):
			edges_to.append(list_of_additions[k])
			weights_new[new_neuron-1][list_of_additions[k]]=np.random.uniform(-1,1)

		self.eft.append(edges_to)

		#need to add new neuron into neurons {the list}
		min_position = 0 #the index of min_pos
		for to_neuron in edges_to:
			for posn in xrange(len(self.neurons)):
				if self.neurons[posn]==to_neuron:
					neuron_posn=posn

			if neuron_posn>min_position:
				min_position=neuron_posn

		#now we have neuron_posn, form a new list
		new_neurons=[]
		for n in xrange(0,min_position):
			new_neurons.append(self.neurons[n])

		new_neurons.append(new_neuron)

		for n in xrange(min_position,len(self.neurons)):
			new_neurons.append(self.neurons[n])

		self.NUM_OF_NEURONS += 1
		self.wfff = copy.deepcopy(weights_new)
		self.weights_from_to=copy.deepcopy(wfff)
		self.neurons = new_neurons

		return self

	def calculate_cumulative_degrees(self):
		#calculate cumulative degree list
		self.cumulative_degrees=[]
		self.cumulative_degrees.append(self.degrees[0])

		for k in xrange(1,len(self.degrees)):
			self.cumulative_degrees.append(self.cumulative_degrees[k-1]+self.degrees[k])
		return self

	def remove_neuron(self):
		self.return_network_properties()
		new_neurons_list=[]
		sub_neurons = [n for n in self.neurons if n not in self.output_neurons if n not in self.input_neurons]

		self.calculate_cumulative_degrees()

		#calculate probability
		maxprob = self.cumulative_degrees[-1]
		proba = np.random.randint(0,maxprob)

		#select neuron
		if proba <= self.cumulative_degrees[0]:
			selected_neuron_index = sub_neurons[0]
		else:
			for k in xrange(len(self.cumulative_degrees)-1):
				if proba > self.cumulative_degrees[k]:
					if proba <= self.cumulative_degrees[k+1]:
						selected_neuron = sub_neurons[k+1]
						selected_neuron_index=k+1


		selected_neuron_index_true=len(self.output_neurons)+selected_neuron_index #for later use...
		#remove neuron
		self.neurons= [n for n in self.neurons if not n==selected_neuron_index_true]

		for to_neuron in self.eft[selected_neuron_index_true]: #at k'th index...
			self.weights_from_to[selected_neuron_index_true][to_neuron]=0

		#set to zero, don't remove from list
		#change etf
		self.etf[selected_neuron_index_true]=[0] #l'echeque this one...

		#change eft
		self.eft[selected_neuron_index_true]=[0] #l'echeque this one...

		#changes biases
		self.biases[selected_neuron_index_true]=0

		#self.NUM_OF_NEURONS-=1

		return self

